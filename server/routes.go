package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/benc-uk/go-starter/pkg/envhelper"
)

type HTTPError struct {
	Error string `json:"error"`
}
type Info struct {
	Environment string `json:"environment"`
	Podname     string `json:"podname"`
}
type Routes struct {
	contentDir  string
	disableCORS bool
}

func (r Routes) apiInfoRoute(resp http.ResponseWriter, req *http.Request) {
	// CORS is for wimps
	if r.disableCORS {
		resp.Header().Set("Access-Control-Allow-Origin", "*")
	}
	resp.Header().Set("Content-Type", "application/json")

	var info Info
	info.Environment = envhelper.GetEnvString("env", "dev")
	info.Podname = envhelper.GetEnvString("podname", "gitops-app")
	js, err := json.Marshal(info)
	if err != nil {
		apiError(resp, http.StatusInternalServerError, err.Error())
		return
	}

	resp.Write(js)
}

func apiError(resp http.ResponseWriter, code int, message string) {
	resp.WriteHeader(code)

	errorData := &HTTPError{
		Error: message,
	}

	errorResp, err := json.Marshal(errorData)
	if err != nil {
		fmt.Printf("### ERROR! httpError unable to marshal to JSON. Message was %s\n", message)
		return
	}
	resp.Write(errorResp)
}

func (r Routes) spaIndexRoute(resp http.ResponseWriter, req *http.Request) {
	http.ServeFile(resp, req, contentDir+"/index.html")
}
