module gitops.com

go 1.15

require (
	github.com/benc-uk/go-starter v1.0.0
	github.com/gorilla/mux v1.8.0
)
