package main

import (
	"fmt"
	"net/http"

	"github.com/benc-uk/go-starter/pkg/envhelper"
	"github.com/gorilla/mux"
)

// spaHandler implements the http.Handler interface, so we can use it
// to respond to HTTP requests. The path to the static directory and
// path to the index file within that static directory are used to
// serve the SPA in the given static directory.

var contentDir string
var serverPort string

func main() {
	serverPort := envhelper.GetEnvString("PORT", "4000")
	contentDir = envhelper.GetEnvString("CONTENT_DIR", ".")
	muxrouter := mux.NewRouter()
	routes := Routes{
		contentDir:  contentDir,
		disableCORS: true,
	}

	muxrouter.HandleFunc("/api/info", routes.apiInfoRoute)

	fileServer := http.FileServer(http.Dir(contentDir))
	muxrouter.PathPrefix("/js").Handler(http.StripPrefix("/", fileServer))
	muxrouter.PathPrefix("/css").Handler(http.StripPrefix("/", fileServer))
	muxrouter.PathPrefix("/img").Handler(http.StripPrefix("/", fileServer))
	muxrouter.PathPrefix("/favicon.ico").Handler(http.StripPrefix("/", fileServer))
	muxrouter.NotFoundHandler = http.HandlerFunc(routes.spaIndexRoute)
	// Start server
	fmt.Printf("### Starting server listening on %v\n", serverPort)
	fmt.Printf("### Serving static content from '%v'\n", contentDir)
	http.ListenAndServe(":"+serverPort, muxrouter)

}
